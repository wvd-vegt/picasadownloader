﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Reflection;

namespace PicasaDownloader
{
    /// <summary>
    /// About Dialog.
    /// </summary>
    public partial class AboutDialog : Form
    {
        /// <summary>
        /// The picasadownloader at bitbucket.
        /// </summary>
        public const String picasadownloader_at_bitbucket = "https://wvd-vegt.bitbucket.io/";

        const String picasadownloader_mailto = "mailto:wim@vander-vegt.nl?SUBJECT=Suggestion for PicasaDownloader";
        const String paypal_picasadownloader = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XQF7SP9FN5AY2";

        /// <summary>
        /// Constructor.
        /// </summary>
        public AboutDialog()
        {
            InitializeComponent();

            textBox1.Text = String.Format(textBox1.Text, Assembly.GetExecutingAssembly().GetName().Version);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(paypal_picasadownloader);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(picasadownloader_at_bitbucket);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(picasadownloader_mailto);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }
    }
}
