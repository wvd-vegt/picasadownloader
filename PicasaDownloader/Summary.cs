﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PicasaDownloader
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Summary : Form
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public Summary()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Checks if there are any messages so it might be neccesary/usefull to show the dialog.
        /// </summary>
        /// <returns>true if there are messages</returns>
        public Boolean HasMessages()
        {
            return listView1.Items.Count != 0;
        }

        /// <summary>
        /// Clears the listview Items and Groups.
        /// </summary>
        public void Clear()
        {
            listView1.Items.Clear();
            listView1.Groups.Clear();
        }

        /// <summary>
        /// Adds an Item to a Group.
        /// </summary>
        /// <param name="group">The name of the group</param>
        /// <param name="origin">The origina causing the problem</param>
        /// <param name="reason">The problem</param>
        public void Add(String group, String origin, String reason)
        {
            ListViewItem lvi = listView1.Items.Add(new ListViewItem(new String[] { origin, reason }));

            foreach (ListViewGroup lvg in listView1.Groups)
            {
                if (lvg.Header.Equals(group, StringComparison.CurrentCultureIgnoreCase))
                {
                    lvg.Items.Add(lvi);

                    return;
                }
            }

            Int32 ndx = listView1.Groups.Add(new ListViewGroup(group));

            listView1.Groups[ndx].Items.Add(lvi);
        }

        /// <summary>
        /// Resize the listview columns to fit content.
        /// </summary>
        public void ResizeColumns()
        {
            foreach (ColumnHeader ch in listView1.Columns)
            {
                ch.Width = -1;
            }
        }
    }
}
