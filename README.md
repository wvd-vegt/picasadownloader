# Project Description #

This application lets you download multiple Picasa web albums of a selected Picasa user with only a few clicks and without Picasa being installed.

If you like this tool, leave me a note, rate this project or write a review or [Donate to PicasaDownloader](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XQF7SP9FN5AY2).

  [![paypal.png](https://bitbucket.org/repo/qE7Kbro/images/1378486-paypal.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=XQF7SP9FN5AY2)

If you've encountered problems, leave a (detailed) issue in the [Issue Tracker](/ghostbuster/issues?status=new&status=open).

PicasaDownloader [installers](https://wvd-vegt.bitbucket.io/).

# Quick Manual #

* Start PicasaDownloaded,
* Enter the Picasa Username, 
* Hit the List button, 
* Select albums to download, 
* Hit Download,
* Take some Coffee!

![Home_PicasaDownloader_1.png](https://bitbucket.org/repo/Ggrx5Rr/images/2784268571-Home_PicasaDownloader_1.png)

# Some hidden gems #

* Right-Click an album for an album preview. This preview can be stored on disk and contains direct links to the thumbnails. 
* Ctrl-Click the download button to download at images at the maximum resolution. 
* Left-Click the album count (bottom right) to open a download directory. 
* Right-Click the album count (bottom right) to select a download directory. 
* PicasaDownloader can access more then 10k albums of a single user. 
* Paste or drop a link of a shared album containing an authkey in the userid edit box.

![Home_PicasaDownloader_2.png](https://bitbucket.org/repo/Ggrx5Rr/images/1687783887-Home_PicasaDownloader_2.png)

# Downloads #

Download [location](https://wvd-vegt.bitbucket.io/PicasaDownloader/PicasaDownloader.application) (Click-Once).

Other downloads [installers](https://wvd-vegt.bitbucket.io/).

# Forking #

I'm NOT a big fan of forking and specifically creating repository copies at GitHub (as the sources end-up all over the place and in various states of decay). So please do not fork. 
If you want to contribute, please post or mail changes for review.

# Latest release #

* Set version to v1.0.25. 
* Corrected install location. 
* Fixed preview for shared albums. 
* Fixed one more DateTime typecast. 
* Disabled debug output. 
* Corrected install location Swiss. 
* Changes PicasaEntry and PicasaAlbumEntry to a class. 
* Added support for shared albums where the url contains a  authkey query parameter.
* These url's can be pasted (or dropped) in  the userid edit field.
* The supported format is: 
* https://picasaweb.google.com/<usedid>/<abumname>?authuser=0&authkey=<authkey>&feat=directlink
* where the authuser and feat parameters are ignored. 
 
# Previous releases #


# Some reviews (after being added to Softpedia, people started to find PicasaDownloader) #

* [http://www.ghacks.net/2009/12/04/google-picasa-downloader/](http://www.ghacks.net/2009/12/04/google-picasa-downloader/)
* [http://www.instantfundas.com/2009/12/download-picasa-albums-without-picasa.html](http://www.instantfundas.com/2009/12/download-picasa-albums-without-picasa.html)
* [http://www.teknobites.com/2009/12/04/google-picasa-web-albums-downloader/](http://www.teknobites.com/2009/12/04/google-picasa-web-albums-downloader/)
* [http://blog.soft.idv.tw/?p=652 can't read Taiwanese](http://blog.soft.idv.tw/?p=652)
* [http://techie-buzz.com/softwares/download-public-picasa-albums-from-desktop.html](http://techie-buzz.com/softwares/download-public-picasa-albums-from-desktop.html)
* [http://www.tothepc.com/archives/download-picasa-web-photo-albums-of-any-user/](http://www.tothepc.com/archives/download-picasa-web-photo-albums-of-any-user/)
* [http://www.addictivetips.com/windows-tips/download-picasa-photo-albums/](http://www.addictivetips.com/windows-tips/download-picasa-photo-albums/)
* [http://www.nirmaltv.com/2009/12/04/download-multiple-picasa-web-albums-easily/](http://www.nirmaltv.com/2009/12/04/download-multiple-picasa-web-albums-easily/)
* [http://www.google.org.cn/posts/picasa-downloader-download-picasa-web-albums.html](http://www.google.org.cn/posts/picasa-downloader-download-picasa-web-albums.html)